/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Stefan Schmidt <stefan.schmidt@huawei.com> */

#include <stdio.h>
#include <modbus.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int ret;
	modbus_t *ctx;
	int device_id = 7;
	uint16_t regs[8];

	if (argc < 2) {
		printf("Usage %s [-tcp] DEVICE | IP\n", argv[0]);
		return -1;
	}

	if (!strcmp(argv[1], "-tcp")) {
		ctx = modbus_new_tcp_pi(argv[2], "1502");
		if (ctx == NULL) {
			fprintf(stderr, "Unable to create the libmodbus context\n");
			return -1;
		}
	} else {
		ctx = modbus_new_rtu(argv[1], 115200, 'N', 8, 1);
		if (ctx == NULL) {
			fprintf(stderr, "Unable to create the libmodbus context\n");
			return -1;
		}
		modbus_set_slave(ctx, device_id);
	}

	modbus_set_debug(ctx, TRUE);

	ret = modbus_connect(ctx);
	if (ret < 0) {
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return -1;
	}

	/* contax, address, registers, destination */
	ret = modbus_read_registers(ctx, 0, 8, regs);
	if(ret < 0){
		fprintf(stderr, "Modbus register read error: %s\n", modbus_strerror(errno));
		modbus_close(ctx);
		modbus_free(ctx);
		return -1;
	}

	printf("Register 7 = %d\n",regs[7]);

	modbus_close(ctx);
	modbus_free(ctx);
	return 0;
}
