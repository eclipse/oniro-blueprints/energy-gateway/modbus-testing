# Building

To build the examples you need to have meson, ninja and libmodbus and its development files installed on your system. 
```
$ meson setup build
$ cd build
$ ninja
```
After a successful build you will have two executables in your build dir: modbus-server and modbus-client

# Running

To test Modbus RTU (Remote Terminal Unit) over an existing RS-485 port (we assume its called /dev/ttyUSB0) you need to start the server on one side:
```
$ ./modbus-server /dev/ttyUSB0
```
And the client on the other side:
```
$ ./modbus-client /dev/ttyUSB0
```

To test Modbus TCP instead you would run the following commands:
```
$ ./modbus-server -tcp
```
And the client on the other side:
```
$ ./modbus-client -tcp SERVER_IP_ADDRESS
```
