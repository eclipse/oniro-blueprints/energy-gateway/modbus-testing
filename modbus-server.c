/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Stefan Schmidt <stefan.schmidt@huawei.com> */

#include <stdio.h>
#include <modbus.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int ret;
	modbus_t *ctx;
	int device_id = 7;
	modbus_mapping_t *mapping;
	uint8_t request[MODBUS_RTU_MAX_ADU_LENGTH];
	int len;
	int socket = -1;

	if (argc < 2) {
		printf("Usage %s [-tcp] [DEVICE]\n", argv[0]);
		return -1;
	}

	if (!strcmp(argv[1], "-tcp")) {
		/* NULL for IP means listen on all addresses */
		ctx = modbus_new_tcp_pi("::1", "1502");
		if (ctx == NULL) {
			fprintf(stderr, "Unable to create the libmodbus context\n");
			return -1;
		}

		socket = modbus_tcp_pi_listen(ctx, 1);
		modbus_tcp_pi_accept(ctx, &socket);
	} else {
		ctx = modbus_new_rtu(argv[1], 115200, 'N', 8, 1);
		if (ctx == NULL) {
			fprintf(stderr, "Unable to create the libmodbus context\n");
			return -1;
		}

		modbus_set_slave(ctx, device_id);
		//ret = modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485);
		ret = modbus_connect(ctx);
		if (ret < 0) {
			fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
			modbus_free(ctx);
			return -1;
		}
	}

	modbus_set_debug(ctx, TRUE);

	/* 0 ouput coil, 1 input coil, 8 holding, registers, 2 input registers */
	//mapping = modbus_mapping_new(MODBUS_MAX_READ_BITS, 0,
	//				MODBUS_MAX_READ_REGISTERS, 0);
	mapping = modbus_mapping_new(0, 1, 8, 2);
	if (mapping == NULL) {
		fprintf(stderr, "Mapping allocation failed: %s\n", modbus_strerror(errno));
		modbus_close(ctx);
		modbus_free(ctx);
	}

	mapping->tab_registers[7] = 42;

	while(1) {
		len = modbus_receive(ctx, request);
		if (len < 0)
			break;

		modbus_reply(ctx, request, len, mapping);
	}

	modbus_mapping_free(mapping);
	modbus_close(ctx);
	modbus_free(ctx);
	return 0;
}
